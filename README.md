# hippo (Dmitriy Popov for Actum)
Test project to learn the basics of Hippo CMS.
Initially based on ["Build a Website" tutorial](https://www.onehippo.org/trails/developer-trail/introduction.html).

Forecast controller is using [forecast for 5 days API-request on openweathermap.org web-service](https://openweathermap.org/forecast5).
Request example: http://api.openweathermap.org/data/2.5/forecast?APPID=e4c97f196fc0b1507c2212460f34ab40&mode=xml&q=London,uk

Creating documents controller works via ``WorkflowPersistenceManager`` and is strongly based [on the example from the documentation](https://www.onehippo.org/library/concepts/component-development/hstcomponent-persistable-annotation-and-workflow.html).


## How to run the project
* Clone the repository.
* Run ``mvn verify``.
* Run ``mvn -Pcargo.run -Drepo.path=storage``. Note that current storage version is included into the sources. 
* After starting the container, current forecast/document creation page can be opened on <http://localhost:8080/site/>.
To create the document in the current version, submit the form on the page. New document will be generated and published.
* To switch form between the controllers (components), go to [``/hst:hst/hst:configurations/myGoGreen/hst:pages/home``](http://localhost:8080/cms/console/?1&path=/hst:hst/hst:configurations/myGoGreen/hst:pages/home) element
  and change ``hst:referencecomponent`` property to ``hst:components/forecastComponent`` or ``hst:components/documentComponent`` accordingly.  
* Press ``Ctrl-C`` to stop the container.


## Main files to view
* ``DocumentComponent`` — контроллер, выполняющий программное создание документов. 
* ``NewsDocument`` — Document Type, используемый для генерации документов. Изначально сгенерирован с помощью ``essentials``
  , но для программной генерации документов модифицирован вручную: добавлены поля и их сеттеры вместо геттеров через ``getProperty``
  , добавлена имплементация интерфейса ``ContentNodeBinder``.  
* ``ForecastComponent`` — контроллер, выполняющий запрос в веб-сервис получения данных о погоде и упакову результатов в атрибуты реквеста, отображаемые на ``home.ftl``.
* ``ForecastClient`` — клиент веб-сервиса погоды. API-key и урл запроса захардкожены в нём. Вызов выполняется средствами [Apache CXF](http://cxf.apache.org/).
* ``home.ftl`` — страница для ввода города для запроса погоды и вывода полученных из веб-сервиса результатов.
* ``forecast.xsd`` — схема для запроса сервиса погоды. Сгенерирована из примера ответов с помощью IDEA.
* ``jaxb-generate.bat`` — батник для генерации клиентских классов веб-сервиса на основе ``forecast.xsd``.


## Current issues and TODOs
* Непонятно, как создавать в документе нескалярные поля, а именно картинки.
* Проект сгенерирован с CamelCase-именем ``myGoGreen``. Однако при создании узлов через ``WorkflowPersistenceManager`` пути преобразуется в строчные буквы.
Происходит это в 
  ```
  WorkflowPersistenceManagerImpl.createAndReturn
  ->
  createNodeByWorkflow
  ->
  String nodeName = uriEncoding.encode(name);
  ```
  В результате документы генерируются не в ``/content/documents/myGoGreen``, а в ``/content/documents/mygogreen``, а это разные пути в репозитории
  , что видно и в контент-консоли, и в древесной консоли.  
  Как следствие, такие документы, очевидно, не видны в поиске документов на сайте.    
  Скорее всего, придётся перегенерировать проект с названием, состоящим только из строчных букв.
* Нужно сделать отдельную страницу для получения прогноза и отдельную, вызывающую генерацию документов.
Соответственно, привязаны они будут к разным контроллерам.
* В ``ForecastComponent`` много мусорного неиспользуемого кода. Нужно почистить.
* Нужно настроить нормальное логирование в контроллерах через настройку slf4j/log4j
, и убрать дублирование в консоль в методах ``ForecastComponent#log``, ``DocumentComponent#log``.


## Known tricky points
* Если выполнить поиск документов в методе контроллера, аннотированном как ``@Persistable``, 
то документы не будут видны ни через ``HstQuery``, ни через иерархию ``HippoBean`` / ``javax.jcr.Node``.
Иерархия директорий ``HippoFolder`` при этом видна. 
* Если в ``WorkflowCallbackHandler`` запускать сначала ``requestPublication()``, а потом ``publish()``
, то вызов ``publish`` падает с исключением
  ```
  org.hippoecm.repository.api.WorkflowException: Cannot invoke workflow documentworkflow action publish: action not allowed or undefined
  ```
  Если вызывать просто ``workflow.publish()``, то публикация отрабатывает.
* Если в xsd-схеме присутствует много атрибутов с одинаковым именем ``value``, то при генерации клиента будет падать следующая ошибка:
  ```
  com.sun.istack.SAXParseException2: Property "Value" is already defined. Use &lt;jaxb:property> to resolve this conflict.  
  ``` 
  Для исправления вставляем внутрь каждого атрибута ``value`` уникальное название для соответствующего JAXB-property:
  ```xml
      <xs:attribute type="xs:string" name="value" use="optional">
        <!-- rename property generated by JAXB (avoiding "Value" name conflict) -->
        <xs:annotation>
          <xs:appinfo>
            <jxb:property name="valueTemperatureType"/>
          </xs:appinfo>
        </xs:annotation>
      </xs:attribute>
   ```
   В этом случае генерация сработает, а в клиентском классе java-поле будет называться не ``value``, а ``valueTemperatureType``.