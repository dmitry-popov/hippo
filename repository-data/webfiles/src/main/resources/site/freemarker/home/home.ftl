<#include "../include/imports.ftl">
<#assign KELVIN_DEGREES_IN_CELSIUM = -273.15>

<html>
<head>
    <title>Actum weather forecast on Hippo CMS</title>

    <style>
        h1.results {
            color: #878069;
        }

        h2.results {
            color: #878069;
            padding-bottom: 0;
            margin-bottom: 0;
        }

        h3.results {
            color: #878069;
            padding-bottom: 0;
            margin-bottom: 0;
        }

        table.results {
            border-collapse: collapse;
        }
        table.results td {
            padding: 0 5px;
            border: 1px solid #DDDDDD;
            text-align: right;
        }
        table.results td.left {
            text-align: left
        }

        p.error {
            color: #ff0000;
        }
    </style>
</head>
<body>
    <h1 class="results">Weather forecast (Dmitriy Popov for Actum)</h1>

    <h2 class="results">Form to submit</h2>
    <@hst.actionURL var="actionLink"/>
    <#--Action link: ${actionLink}-->
    <form action="${actionLink}" method="post">
        <label for="form-text-input">Town and country</label>:&nbsp;<input id="form-text-input" type="text" name="town"/>
        <br/>
        <input type="submit" value="Submit form"/>
    </form>

    <#if errorText??>
        <p class="error">
          Error: ${errorText}
        </p>
    <#elseIf noFormSubmitted??>
        <p>Fill in the city and submit the form.</p>
    <#else>
        <h2 class="results">Location</h2>
        <table class="results">
            <tbody>
                <#list locationContentMap?keys as key>
                    <tr>
                        <td class="left">${key}</td>
                        <td>${locationContentMap[key]}</td>
                    </tr>
                </#list>
                <#if latitude??>
                    <tr>
                        <td class="left">Latitude</td>
                        <td>${latitude}</td>
                    </tr>
                </#if>
                <#if longitude??>
                    <tr>
                        <td class="left">Longitude</td>
                        <td>${longitude}</td>
                    </tr>
                </#if>
            </tbody>
        </table>

        <h2 class="results">Sun</h2>
        <table class="results">
            <tbody>

                <#if sunrise??>
                    <tr>
                        <td class="left">Sunrise</td>
                        <td>${sunrise?datetime.iso?string("dd.MM.yyyy HH:mm:ss")}</td>
                    </tr>
                </#if>

                <#if sunset??>
                    <tr>
                        <td class="left">Sunset</td>
                        <td>${sunset?datetime.iso?string("dd.MM.yyyy HH:mm:ss")}</td>
                    </tr>
                </#if>

            </tbody>
        </table>

        <h2 class="results">Forecast</h2>
        <#list forecastTimeList as forecast>
           <h3 class="results">${forecast.from?datetime.iso?string("dd.MM.yyyy HH:mm:ss")} &mdash; ${forecast.to?datetime.iso?string("dd.MM.yyyy HH:mm:ss")}</h3>

            <table class="results">
                <tbody>

                <#if forecast.symbol??>
                    <tr>
                        <td class="left">Description</td>
                        <td>${forecast.symbol.name}</td>
                    </tr>
                </#if>

                <#if forecast.windDirection??>
                    <tr>
                        <td class="left">Wind direction</td>
                        <td>${forecast.windDirection.deg}&deg; ${forecast.windDirection.name}</td>
                    </tr>
                </#if>

                <#if forecast.windSpeed??>
                    <tr>
                        <td class="left">Wind speed</td>
                        <td>
                            ${forecast.windSpeed.mps} mps

                            <#--<#if forecast.windSpeed.name?? && forecast.windSpeed.name.hasContent>-->
                            <#if forecast.windSpeed.name?hasContent>
                                &nbsp;(${forecast.windSpeed.name})
                            </#if>
                        </td>
                    </tr>
                </#if>

                <#if forecast.temperature??>
                    <tr>
                        <td class="left">Temperature</td>

                        <#-- todo: check whether unit is "kelvin" and compute according to the unit -->
                        <td>
    <#--
                            Value: ${forecast.temperature.valueTemperatureType}
                            <br/>
                            Unit: ${forecast.temperature.unit}
                            <br/>
                            Min: ${forecast.temperature.min}
                            <br/>
                            Max: ${forecast.temperature.max}
                            <br/>
    -->

                            ${forecast.temperature.valueTemperatureType?number + KELVIN_DEGREES_IN_CELSIUM}&deg;
                            (min: ${forecast.temperature.min?number + KELVIN_DEGREES_IN_CELSIUM}&deg;,&nbsp;max: ${forecast.temperature.max?number + KELVIN_DEGREES_IN_CELSIUM}&deg;)
                        </td>
                    </tr>
                </#if>

                <#if forecast.pressure??>
                    <tr>
                        <td class="left">Pressure</td>
                        <td>
                            ${forecast.pressure.valuePressureType}&nbsp;${forecast.pressure.unit}
                        </td>
                    </tr>
                </#if>

                <#if forecast.humidity??>
                    <tr>
                        <td class="left">Humidity</td>
                        <td>
                            ${forecast.humidity.valueHumidityType}&nbsp;${forecast.humidity.unit}
                        </td>
                    </tr>
                </#if>

                <#if forecast.clouds??>
                    <tr>
                        <td class="left">Clouds</td>
                        <td>
                            ${forecast.clouds.valueCloudsType}&nbsp;(${forecast.clouds.all}&nbsp;${forecast.clouds.unit})
                        </td>
                    </tr>
                </#if>


                </tbody>
            </table>
        </#list>
    </#if>

</body>
</html>