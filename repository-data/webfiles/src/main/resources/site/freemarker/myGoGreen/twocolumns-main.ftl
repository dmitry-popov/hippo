<#include "../include/imports.ftl">
<div class="body-wrapper">
  <div class="container">
    <div class="row">


      <#-- this is the left column from twocolumns-main.ftl -->
      <div class="col-md-9 col-sm-9">
          Two columns left
        <@hst.include ref="left"/>
      </div>

      <#-- this is the right column from twocolumns-main.ftl -->
      <div class="col-md-3 col-sm-3">
          Two columns right
        <@hst.include ref="right"/>
      </div>

    </div>
  </div>
</div>