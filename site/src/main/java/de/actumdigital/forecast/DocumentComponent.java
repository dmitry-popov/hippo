package de.actumdigital.forecast;

import org.apache.jackrabbit.JcrConstants;
import org.example.beans.Imageset;
import org.example.beans.NewsDocument;
import org.hippoecm.hst.component.support.bean.BaseHstComponent;
import org.hippoecm.hst.content.annotations.Persistable;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.manager.workflow.BaseWorkflowCallbackHandler;
import org.hippoecm.hst.content.beans.manager.workflow.WorkflowPersistenceManager;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.content.beans.standard.HippoFolder;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageBean;
import org.hippoecm.hst.core.component.HstComponentException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.repository.documentworkflow.DocumentWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import java.io.*;
import java.util.Calendar;
import java.util.Date;

public class DocumentComponent extends BaseHstComponent {
    private static final Logger logger = LoggerFactory.getLogger(DocumentComponent.class);
    private static final String FILES_SAVE_DIRECTORY_PATH = "C:/java/hippo/__from-hippo/";

    private void log(final String s) {
        System.out.println("[DocumentComponent]: " + s); // todo: remove this
        logger.info(s);
    }
    private void logError(final String message, final Throwable t) {
        System.out.println("[DocumentComponent][ERROR]: " + message); // todo: remove this
        t.printStackTrace(); // todo: remove this

        logger.error(message, t);
    }

    @Override
    @Persistable
    public void doAction(final HstRequest request, final HstResponse response) throws HstComponentException {
        log("-------------------- doAction (without @Persistable) --------------------");

        super.doAction(request, response);

        createDocument(request);

//        findDocument(request); // findDocument won't work if @Persistable annotation is present
        log("-------------------- doAction finished --------------------");
    }

    @Override
    public void doBeforeRender(final HstRequest request, final HstResponse response) throws HstComponentException {
        log("-------------------- doBeforeRender --------------------");
        super.doBeforeRender(request, response);

        request.setAttribute("errorText", "This is DocumentComponent. Don't panic."); // prevent ftl-errors in the console

        findDocument(request); // todo: uncomment to execute the documents search

        log("-------------------- doBeforeRender finished --------------------");
    }

    private void createDocument(final HstRequest request) {
        log("-------------------- createDocument --------------------");


        try {
            final HstRequestContext requestContext = request.getRequestContext();
            final Session session = requestContext.getSession();

            final WorkflowPersistenceManager workflowPersistenceManager = getWorkflowPersistenceManager(session);

            // register callbackHandler // todo: think whether it is required if we don't want to publish the document, only create it
            workflowPersistenceManager.setWorkflowCallbackHandler(new BaseWorkflowCallbackHandler<DocumentWorkflow>() {
                public void processWorkflow(final DocumentWorkflow workflow) throws Exception {
/*
                    workflow.requestPublication();
                    log("[WorkflowCallbackHandler]: requestPublication executed");
*/

                    // workflow.publish() after workflow.requestPublication()
                    // causes throwing of "org.hippoecm.repository.api.WorkflowException: Cannot invoke workflow documentworkflow action publish: action not allowed or undefined"
                    workflow.publish( new Date() );
                    log("[WorkflowCallbackHandler]: publish executed");
                }
            });
            log("WorkflowCallbackHandler has been set");

            // todo: use requestContext.getSiteContentBaseBean() if possible
            // todo: use news path instead
            // it is not important where we store comments. WE just use some timestamp path below our project content
//            final String siteCanonicalBasePath = request.getRequestContext().getResolvedMount().getMount().getContentPath();
            final String siteCanonicalBasePath = requestContext.getSiteContentBasePath();

            final String newsFolderPath = "/" + siteCanonicalBasePath + "/news/2018/08"; // initial "/" is required to not have "Not an absolute path" thrown
            log("newsFolderPath (point 1): " + newsFolderPath);

//            final Calendar currentDate = Calendar.getInstance();
//            final String newsFolderPath = siteCanonicalBasePath + "/comment/"
//                    + currentDate.get(Calendar.YEAR) + "/"
//                    + currentDate.get(Calendar.MONTH) + "/"
//                    + currentDate.get(Calendar.DAY_OF_MONTH)
//            ;

            // comment node name is simply a concatenation of 'comment-' and current time millis.
            final String newsNodeName = "news-for-" + "dmitriy-popov" + "-" + System.currentTimeMillis(); // !!! no big letters here, otherwise it won't be found by wpm#getObject
            log("newsNodeName: " + newsNodeName);

            // todo: note that will replace myGoGreen -> mygogreen in WPMImpl#createAndReturn -> WorkflowPersistenceManagerImpl.createMissingFolders -> uriEncoding.encode(folderName)
            // create comment node now
            log("Executing createAndReturn...");
            workflowPersistenceManager.createAndReturn(
                  newsFolderPath
                , "myGoGreen:newsdocument" // from @Node(jcrType=) // todo: use as constant or parse the annotation
                , newsNodeName
                , true
            );
            log("createAndReturn executed");

            // retrieve the comment content to manipulate
            final String path = newsFolderPath + "/" + newsNodeName;

            // Not an absolute path: content/documents/myGoGreen/news/2018/08
            // content/documents/mygogreen/comment/2018/7//3/news-for-dpopov-1533295538729/news-for-dpopov-1533295538729/hippo:availability
            log("full node path: " + path);

            final String pathLowerCase = path.toLowerCase(); // a trick to work with "mygogreen" instead of "myGoGreen", since wpm#createAndReturn replaces path parts with lowercase letters (see the comment above)
            log("full node path LOWERCASE: " + pathLowerCase);

            final NewsDocument newsDocument = (NewsDocument) workflowPersistenceManager.getObject(pathLowerCase);
            log("newsDocument after calling wpm#getObject: " + newsDocument);
            // update content properties
            if (newsDocument == null) {
                throw new HstComponentException("Failed to add NewsDocument. WPM#getObject returned null.");
            }

            newsDocument.setTitle("Test title for created document");
            newsDocument.setDate( Calendar.getInstance() );
            newsDocument.setIntroduction("Test introduction");
            newsDocument.setLocation("Test location");
            newsDocument.setAuthor("Test author");
            newsDocument.setSource("Test source");

            // todo: set non-scalar fields too, especially Image


            // update now
            log("Updating newsDocument...");
            workflowPersistenceManager.update(newsDocument);
            log("newsDocument has been updated.");
        }
        catch (final RepositoryException | ObjectBeanManagerException e) {
            logError("Error in createDocument", e);
        }

        log("-------------------- createDocument finished --------------------");
    }


    private void findDocument(final HstRequest request) {
        final HstRequestContext requestContext = request.getRequestContext();

        try {
/*
            final Session session = requestContext.getSession();

            final Node node = session.getNodeByIdentifier(); // javax.jcr.Node
            final String nodePath = node.getPath();


            final WorkflowPersistenceManager workflowPersistenceManager = getWorkflowPersistenceManager(session);

            final HippoBean contentBean = requestContext.getContentBean();
            getContentBean(request);


            workflowPersistenceManager.getWorkflow();

            workflowPersistenceManager.createAndReturn();

            workflowPersistenceManager.update();
*/

            // path to search
//            final Node scopeNode = ;
            final HippoBean siteScope = requestContext.getSiteContentBaseBean();

            final Node siteScopeNode = siteScope.getNode();
            final NodeIterator siteChildNodesIterator = siteScopeNode.getNodes();

            while ( siteChildNodesIterator.hasNext() ) {
                final Node node = siteChildNodesIterator.nextNode();
                log( String.format("site child node: %s", node.getPath()) );
            }


            final HippoFolder news = siteScope.getBean("news");
            final HippoFolder news_2018 = news.getBean("2018");
            final HippoFolder news_2018_07 = news_2018.getBean("07");

            news_2018_07.getDocuments(); // no success, empty list
            news_2018_07.getDocumentIterator(NewsDocument.class); // empty iterator

            final Node news_2018_07_Node = news_2018_07.getNode();
            final NodeIterator nodes = news_2018_07_Node.getNodes();
            while ( nodes.hasNext() ) {
                final Node node = nodes.nextNode();

                log("------------------------------------");
                log( String.format("News/2018/07 child node. [Path: %s, name: %s]", node.getPath(), node.getName()) );

                final NodeIterator childNodes = node.getNodes();
                log( String.format( "childNodes.hasNext(): %b", childNodes.hasNext() ) );
                log("------------------------------------");
            }

//            final String path = "/"; // todo: what must be here?
//            final HippoBean scope = siteScope.getBean(path, HippoFolder.class);

            final HstQueryManager queryManager = requestContext.getQueryManager();

            // todo: maybe use HippoBean instead of Node
//            final HstQuery query = queryManager.createQuery(siteScope, BaseDocument.class, true);
//            final HstQuery query = queryManager.createQuery(siteScope, HippoDocument.class, true);
            final HstQuery query = queryManager.createQuery(news_2018_07, NewsDocument.class, true);
            query.setLimit(10);

            final String queryAsString = query.getQueryAsString(false);
            log( String.format("Query as string: %s", queryAsString) );

            log("Executing query...");
            final HstQueryResult queryResult = query.execute();
            log("Query executed successfully.");

            final HippoBeanIterator beanIterator = queryResult.getHippoBeans();
            log( String.format("Iterator.size: %d", beanIterator.getSize()) );
            log( String.format("Iterator.hasNext: %b", beanIterator.hasNext()) );

            while (beanIterator.hasNext()) {
                final HippoBean hippoBean = beanIterator.nextHippoBean();
                hippoBean.getDisplayName();

                log("============= HippoBean =============");
                log( String.format("name: %s" , hippoBean.getName()) );
                log( String.format("displayName: %s" , hippoBean.getDisplayName()) );
                log( String.format("path: %s" , hippoBean.getPath()) );
                log( String.format("comparePath: %s" , hippoBean.getComparePath()) );

                final NewsDocument newsDocument = (NewsDocument) hippoBean;
                final Imageset image = newsDocument.getImage();
                if (image == null) {
                    log("Document has no image.");
                }
                else {
                    final HippoGalleryImageBean imageOriginal = image.getOriginal();
                    log( String.format("image path: %s", image.getPath()) );
                    log( String.format("image file name: %s", image.getFileName()) );
                    log( String.format("image original path: %s", imageOriginal.getPath()) );
                    log( String.format("image original name: %s", imageOriginal.getName()) );
                    log( String.format("image original mime type: %s", imageOriginal.getMimeType()) ); // method of HippoResourceBean
                    log( String.format("image original width: %d", imageOriginal.getWidth()) ); // method of HippoResourceBean
                    log( String.format("image original height: %d", imageOriginal.getHeight()) ); // method of HippoResourceBean
                    log( String.format("image original size bytes: %d", imageOriginal.getLength()) ); // method of HippoResourceBean

                    // seems to be no way of get image bytes via Hippo objects, but with jcr node propeties it is possible
                    final Node imageOriginalNode = imageOriginal.getNode();
                    final Property jcrData = imageOriginalNode.getProperty(JcrConstants.JCR_DATA);
                    final Binary binary = jcrData.getBinary();
                    log( String.format( "image original binary size: %d", binary.getSize()) );

//                    saveImageToFile(image, binary); // uncomment this to save images to disk
                }

                log("=====================================");
            }
        }
/*
        catch (final RepositoryException e) {
            logger.error("Error on getting Session", e);
            e.printStackTrace(); // todo: remove this
        }
*/
        catch (final QueryException e) {
            logError("Error on query execution", e);
        }
        catch (RepositoryException e) {
            logError("Repository exception", e);
        }
    }

    private void saveImageToFile(final Imageset image, final Binary binary) throws RepositoryException {
        final String filePath = FILES_SAVE_DIRECTORY_PATH + image.getFileName() ;

        try {
            final InputStream in = binary.getStream();
            final byte[] buffer = new byte[in.available()];
            final int bytesRead = in.read(buffer);
            log( String.format("Bytes read from image: %d", bytesRead) );

            final File targetFile = new File(filePath);
            final OutputStream out = new FileOutputStream(targetFile);
            out.write(buffer);
            log( String.format("Successfully written image original content to file \"%s\"", filePath) );
        }
        catch (final IOException e) {
            logError( String.format("Error while writing to file %s", filePath), e );
        }
    }
}