package de.actumdigital.forecast;

import de.actumdigital.forecast.client.WeatherdataType;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class ForecastClient {
    private static final Logger logger = LoggerFactory.getLogger(ForecastClient.class);

    private String baseAddress = "http://api.openweathermap.org/";
    private String path = "/data/2.5/forecast";
    private long connectionTimeout = 5000L;
    private long receiveTimeout = 5000L;

    private static final String API_KEY_PARAMETER_NAME = "APPID";
    private static final String API_KEY = "e4c97f196fc0b1507c2212460f34ab40"; // todo: it's better to read it from some configuration file
    private static final String MODE_PARAMETER_NAME = "mode";
    private static final String MODE_PARAMETER_VALUE = "xml";
    private static final String QUERY_PARAMETER_NAME = "q";

    private void log(final String s) {
        System.out.println("[ForecastClient]: " + s); // todo: remove this
        logger.info(s);
    }

    // http://api.openweathermap.org/data/2.5/forecast?mode=xml&q=London,uk&APPID=e4c97f196fc0b1507c2212460f34ab40
    // http://api.openweathermap.org/data/2.5/forecast?APPID=e4c97f196fc0b1507c2212460f34ab40&mode=xml&q=London,uk

    public WeatherdataType getWeatherForecast(final String query) {
        final WebClient webClient = WebClient
                .create(baseAddress)
                .path(path)
                .accept(MediaType.APPLICATION_XML)
                .query(API_KEY_PARAMETER_NAME, API_KEY)
                .query(MODE_PARAMETER_NAME, MODE_PARAMETER_VALUE)
                .query(QUERY_PARAMETER_NAME, query);
        setTimeouts(webClient, connectionTimeout, receiveTimeout);

        turnOnRequestLogging(webClient); // todo: this must turned on/off by some config property

        log( String.format("Sending request to %s%s...", baseAddress, path) );

        // todo: maybe catch the exceptions (like timeout, for instance)
        return webClient.get(WeatherdataType.class);
    }

    private void turnOnRequestLogging(final WebClient client) {
        final ClientConfiguration config = WebClient.getConfig(client);
        config.getInInterceptors().add(new LoggingInInterceptor());
        config.getOutInterceptors().add(new LoggingOutInterceptor());

        Client client2 = ClientBuilder.newBuilder().register(LoggingFeature.class).build();

        log("request and response logging has been turned on.");
    }

    private void setTimeouts(final WebClient client, final long connectionTimeout, final long receiveTimeout) {
        final HTTPConduit conduit = WebClient.getConfig(client).getHttpConduit();
        if (receiveTimeout != 0) {
            conduit.getClient().setReceiveTimeout(receiveTimeout);
        }

        if (connectionTimeout != 0) {
            conduit.getClient().setConnectionTimeout(connectionTimeout);
        }
    }

    public String getBaseAddress() {
        return baseAddress;
    }
    public void setBaseAddress(String baseAddress) {
        this.baseAddress = baseAddress;
    }

    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }

    public long getConnectionTimeout() {
        return connectionTimeout;
    }
    public void setConnectionTimeout(final long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public long getReceiveTimeout() {
        return receiveTimeout;
    }
    public void setReceiveTimeout(final long receiveTimeout) {
        this.receiveTimeout = receiveTimeout;
    }
}