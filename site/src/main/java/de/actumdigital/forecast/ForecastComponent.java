package de.actumdigital.forecast;

import de.actumdigital.forecast.client.*;
import org.hippoecm.hst.component.support.bean.BaseHstComponent;
import org.hippoecm.hst.component.support.forms.FormField;
import org.hippoecm.hst.component.support.forms.FormMap;
import org.hippoecm.hst.component.support.forms.FormUtils;
import org.hippoecm.hst.core.component.HstComponentException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.site.HstServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.xml.bind.JAXBElement;
import java.io.Serializable;
import java.util.*;

public class ForecastComponent extends BaseHstComponent {

    private static final Logger logger = LoggerFactory.getLogger(ForecastComponent.class);

    private void log(final String s) {
        System.out.println("[ForecastComponent]: " + s); // todo: remove this
        logger.info(s);
    }

    @Override
    public void doAction(final HstRequest request, final HstResponse response) throws HstComponentException {
        log("I am in doAction!!! Form must have been submitted.");

        super.doAction(request, response);

        final Map<String, String[]> parameterMap = request.getParameterMap();
        final String[] townValues = parameterMap.get("town");
        log( String.format("town parameter value: %s", townValues[0]) );

        request.setAttribute("doActionAttribute", "doActionAttributeValue");

        final FormMap map = new FormMap(request, new String[]{ "town", "doActionAttribute" });

        // hack to not use the same form twice
        final FormField field = new FormField("sealed");
        field.setValueList(Collections.singletonList("false") );
        map.addFormField(field);

        FormUtils.persistFormMap(request, response, map, null);

//        HstResponseUtils.sendRedirect(request, response, "/"); // todo: get path by hippo's siteitem refId
//        log("redirect sent from doAction");
    }

    @Override
    public void doBeforeRender(final HstRequest request, final HstResponse response) throws HstComponentException {
        super.doBeforeRender(request, response);

        log("I am in doBeforeRender!!!");

        final FormMap map = new FormMap();
        FormUtils.populate(request, map);
        log( String.format("map.fieldNames: %s", Arrays.toString( map.getFieldNames() ) ) );

        log( String.format( "map.isSealed: %b", map.isSealed() ) );

        final FormField townFieldFromMap = map.getField("town");
        if (townFieldFromMap == null) { // no parameter -> this was not a form submit, but entering the page
            request.setAttribute("noFormSubmitted", "true");
            return;
        }

        final String town = townFieldFromMap.getValue();
        log( String.format("town value in doBeforeRender: %s", town) );

        final String sealedValue = map.getField("sealed").getValue();
        log( String.format("sealedValue value in doBeforeRender: %s", sealedValue) );

        if (sealedValue.equalsIgnoreCase("true")) {
            // todo: maybe set some special value
            log("Form is already sealed. Do not make the same request again.");
            return;
        }

        // hack to not use the same form twice
        final FormField field = new FormField("sealed");
        field.setValueList(Collections.singletonList("true") );
        map.addFormField(field);
        FormUtils.persistFormMap(request, response, map, null); // since form.setSealed(true) does not work, use this hack


        final HstRequestContext ctx = request.getRequestContext();

        log("Getting weather forecast...");

        final ForecastClient forecastClient = HstServices.getComponentManager().getComponent(ForecastClient.class);


        final WeatherdataType weatherDataType; // todo: use values selected on web-UI
        try {
            weatherDataType = forecastClient.getWeatherForecast(town);
            log("Weather forecast gotten successfully.");
        }
        catch (final BadRequestException e) { // city not set
            logger.error("Error on getting weather forecast", e);
            request.setAttribute("errorText", "City name must be non-empty"); // todo: move parameter name to constant
            return;
        }
        catch (final NotFoundException e) { // city not found
            logger.error("Error on getting weather forecast", e);
            request.setAttribute("errorText", String.format("City \"%s\" is not found", town) ); // todo: move parameter name to constant
            return;
        }
        catch (final Throwable e) { // unknown error
            logger.error("Error on getting weather forecast", e);
            request.setAttribute("errorText", String.format("Unknown error while getting weather forecast: %s", e.getMessage()) ); // todo: move parameter name to constant
            return;
        }

        final LocationType location = weatherDataType.getLocation();
        final List<Serializable> locationContent = location.getContent();
        final Map<String, String> locationContentMap = new LinkedHashMap<>(); // to save parameters insertion order

        for (final Serializable serializable : locationContent) {
            final JAXBElement element = (JAXBElement) serializable;
            final String name = element.getName().toString();
            final String value = element.getValue().toString();
            final Class declaredType = element.getDeclaredType();
            final boolean isLocationType = ( declaredType.getName().equals(LocationType.class.getName()) );
            log( String.format( "Location.%s: %s (type: %s)", name, value, declaredType) );

            if (isLocationType) { // special treatment - use fields of
                final LocationType locationType = (LocationType) element.getValue();

                // only fields that matters are latitude and longitude
                setAttributeIfNotEmpty( request, "latitude", locationType.getLatitude() );
                setAttributeIfNotEmpty( request, "longitude", locationType.getLongitude() );
            }
            else { // simple plain field -> just add it to the map if it has non-empty value
                if ( !isEmpty(value) ) {
                    locationContentMap.put( StringUtils.capitalize(name), value );
                }
            }
        }

        final SunType sun = weatherDataType.getSun();
        final String sunrise = sun.getRise();
        final String sunset = sun.getSet();
        log("sunrise: " + sunrise);
        log("sunset: " + sunset);

        final ForecastType forecast = weatherDataType.getForecast();
        final List<TimeType> timeList = forecast.getTime();

        setAttributeIfNotEmpty(request, "forecastTimeList", timeList);

/*
<time from="2018-07-25T21:00:00" to="2018-07-26T00:00:00">
    <symbol number="800" name="clear sky" var="01n"/>

    <precipitation/>
    <precipitation unit="3h" value="0.18" type="rain"/>

    <windDirection deg="124.5" code="SE" name="SouthEast"/>
    <windSpeed mps="1.57" name=""/>
    <temperature unit="kelvin" value="290.01" min="290.01" max="293.236"/>
    <pressure unit="hPa" value="1021.52"/>
    <humidity value="62" unit="%"/>
    <clouds value="clear sky" all="0" unit="%"/>
</time>
*/

        for (final TimeType timeType : timeList) {
            final String timeTypeFrom = timeType.getFrom();
            final String timeTypeTo = timeType.getTo();

            final SymbolType symbol = timeType.getSymbol();
            final String symbolName = symbol.getName();

            final PrecipitationType precipitation = timeType.getPrecipitation();
            final String precipitationUnit = precipitation.getUnit();
            final String precipitationValue = precipitation.getValue();
            final String precipitationType = precipitation.getType();

            final WindDirectionType windDirection = timeType.getWindDirection();
            final String windDirectionDeg = windDirection.getDeg();
            final String windDirectionCode = windDirection.getCode();
            final String windDirectionName = windDirection.getName();

            final WindSpeedType windSpeed = timeType.getWindSpeed();
            final String windSpeedMps = windSpeed.getMps();

            final TemperatureType temperature = timeType.getTemperature();
            final String temperatureUnit = temperature.getUnit();
            final String temperatureValue = temperature.getValue();
            final String temperatureMin = temperature.getMin();
            final String temperatureMax = temperature.getMax();

            final PressureType pressure = timeType.getPressure();
            final String pressureUnit = pressure.getUnit();
            final String pressureValue = pressure.getValue();

            final HumidityType humidity = timeType.getHumidity();
            final String humidityUnit = humidity.getUnit();
            final String humidityValue = humidity.getValue();

            final CloudsType clouds = timeType.getClouds();
            final String cloudsValue = clouds.getValue();
        }

        // todo: set results to request attributes

        setAttributeIfNotEmpty(request, "locationContentMap", locationContentMap);
        setAttributeIfNotEmpty(request, "sunset", sunset);
        setAttributeIfNotEmpty(request, "sunrise", sunrise);

        log("Request attributes have been set.");

/*

        // todo: get input parameters from the request (city etc)
        // Retrieve the document based on the URL
        Simpledocument document = (Simpledocument) ctx.getContentBean();

        if (document != null) {
            // Put the document on the request
            request.setAttribute("document", document);
        }
*/

        map.setSealed(true); // invalidate the form
        log("FormMap has been sealed.");
    }

    private static boolean isEmpty(final String s) {
        if (s == null) {
            return true;
        }

        if ( s.trim().isEmpty() ) {
            return true;
        }

        return false;
    }

    private void setAttributeIfNotEmpty(final ServletRequest request, final String name, final Object value) {
        if ( value == null )
            return; // do not add empty values

        request.setAttribute(name, value);
    }
    private void setAttributeIfNotEmpty(final ServletRequest request, final String name, final String value) {
        if ( isEmpty(value) )
            return; // do not add empty values

        request.setAttribute(name, value);
    }
}