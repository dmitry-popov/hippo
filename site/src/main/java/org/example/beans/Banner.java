package org.example.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.example.beans.Imageset;

@HippoEssentialsGenerated(internalName = "myGoGreen:bannerdocument")
@Node(jcrType = "myGoGreen:bannerdocument")
public class Banner extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "myGoGreen:title")
    public String getTitle() {
        return getProperty("myGoGreen:title");
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:content")
    public HippoHtml getContent() {
        return getHippoHtml("myGoGreen:content");
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:link")
    public HippoBean getLink() {
        return getLinkedBean("myGoGreen:link", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:image")
    public Imageset getImage() {
        return getLinkedBean("myGoGreen:image", Imageset.class);
    }
}
