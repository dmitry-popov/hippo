package org.example.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageBean;

@HippoEssentialsGenerated(internalName = "myGoGreen:imageset")
@Node(jcrType = "myGoGreen:imageset")
public class Imageset extends HippoGalleryImageSet {
    @HippoEssentialsGenerated(internalName = "myGoGreen:small")
    public HippoGalleryImageBean getSmall() {
        return getBean("myGoGreen:small", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:large")
    public HippoGalleryImageBean getLarge() {
        return getBean("myGoGreen:large", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:smallsquare")
    public HippoGalleryImageBean getSmallsquare() {
        return getBean("myGoGreen:smallsquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:mediumsquare")
    public HippoGalleryImageBean getMediumsquare() {
        return getBean("myGoGreen:mediumsquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:largesquare")
    public HippoGalleryImageBean getLargesquare() {
        return getBean("myGoGreen:largesquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "myGoGreen:banner")
    public HippoGalleryImageBean getBanner() {
        return getBean("myGoGreen:banner", HippoGalleryImageBean.class);
    }
}
